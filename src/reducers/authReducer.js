import { SET_AUTH_TOKEN, SET_AUTH_LOADING } from '../constants';
const initialState = {
    token: undefined,
    loading: false,
};
const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_AUTH_TOKEN:
            return {
                ...state,
                token: action.payload
            };
        case SET_AUTH_LOADING:
            return {
                ...state,
                loading: action.payload
            };
        default:
            return state;
    }
}
export default authReducer;