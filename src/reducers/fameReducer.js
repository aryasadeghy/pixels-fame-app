import { SET_SINGLE_FAME, SET_FAMES, FAMES_LOADING, ADD_FAV, REMOVE_FAV } from '../constants';
const initialState = {
    data: [],
    loading: false,
    single: null,
    favs: []
};
const fameReducer = (state = initialState, action) => {
    switch (action.type) {
        case FAMES_LOADING:
            return {
                ...state,
                loading: action.payload
            };
        case SET_FAMES:
            return {
                ...state,
                data: action.payload
            };
        case SET_SINGLE_FAME:
            return {
                ...state,
                single: action.payload
            };
        case ADD_FAV:
            return {
                ...state,
                favs: [...state.favs, action.payload]
            };
        case REMOVE_FAV:
            return {
                ...state,
                favs: action.payload
            };
        default:
            return state;
    }
}
export default fameReducer;