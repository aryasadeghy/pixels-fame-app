import { combineReducers } from 'redux';
import authReducer from "./authReducer";
import fameReducer from "./fameReducer";


const rootReducer = combineReducers(
    {
        auth: authReducer,
        fames: fameReducer
    }
);

export default rootReducer