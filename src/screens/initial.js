import React, { Component } from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { Container, Footer, Content } from 'native-base'
import AsyncStorage from '@react-native-community/async-storage';
import { getVersion } from 'react-native-device-info';

import config from "../../config";

export default class Initial extends Component {
    async componentDidMount() {
        const { navigation } = this.props;
        try {
            // AsyncStorage.clear();
            let token = await AsyncStorage.getItem(config.StorageToken);
            if (token !== null) {
                navigation.navigate("actors");
            } else {
                navigation.navigate("login");
            }
        } catch (e) {
            navigation.navigate("login");
        }
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.container}>
                        <Image source={{ uri: "https://pngimage.net/wp-content/uploads/2018/05/fame-png-3.png" }} style={styles.imageLogo} />

                    </View>
                </Content>
                <Footer style={styles.footeStyle}>
                    <Text> <Text style={styles.welcome}>version : {getVersion()} </Text></Text>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    welcome: {
        fontSize: 18,
    },
    footeStyle: {
        backgroundColor: '#fff',
        borderColor: '#fff'
    },
    imageLogo: {
        width: 300,
        height: 400,
        resizeMode: 'contain'
    },
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
});
