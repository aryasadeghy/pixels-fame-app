import React from 'react';
import LoginForm from '../components/loginForm'
import { Container, Header, Content, Right, Left, Body, Title, } from 'native-base'
import { Text } from 'react-native';
import Clock from '../components/clock'

const Login = () => {
    return (
        <Container>
            <Header>
                <Left></Left>
                <Body>
                    <Title>Login</Title>
                </Body>
                <Right></Right>
            </Header>
            <Content>
                <Clock />
                <LoginForm />
            </Content>
        </Container>
    );
};
export default Login;
