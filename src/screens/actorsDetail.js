import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View, Text, Image } from 'react-native';
import fameActions from '../actions/fames';
import Spinner from '../components/spinner';
import { Card, CardItem, Left, Right, Body, Thumbnail, Button, Icon, Container, Header, Content } from 'native-base'
import { sin } from 'react-native-reanimated';
import Clock from '../components/clock';

class ActorDetail extends Component {
    state = {
        fav: false
    }
    async componentDidMount() {
        const { navigation, getSingle, single, favs } = this.props
        const id = await navigation.getParam('id')
        await getSingle(id)
        const isFav = favs.find(item => item==id)
        console.log(isFav,'isFav')
        if(isFav){
            this.setState({fav:true})
        }
    }

    onFavClick = () => {
        const { addFav, removeFav, single } = this.props;
        const { fav} = this.state
        if (!fav) {
            addFav(single.id)
        } else {
            removeFav(single.id)
        }
        this.setState({ fav: !fav })

    }
    render() {
        const { single, loading } = this.props
        const { fav } = this.state
        return (
            <Container>
                <Content>
                    <Clock />
                    {single && !loading ?
                        <Card style={{ flex: 0 }}>
                            <CardItem>
                                <Left>
                                    <Thumbnail source={{ uri: single.image }} />
                                    <Body>
                                        <Text>{single.name}</Text>
                                        <Text note>{single.dob}</Text>
                                    </Body>
                                </Left>
                            </CardItem>
                            <CardItem>
                                <Body>
                                    <Image source={{ uri: single.image }} style={{ height: 200, width: 200, flex: 1 }} />
                                    <Text>
                                        Acotor name is {single.name}
                                    </Text>
                                </Body>
                            </CardItem>
                            <CardItem>
                                <Body>
                                    <Button transparent onPress={this.onFavClick}>
                                        <Text> {!fav ? 'Add To Fav' : 'Remove From Fav'} </Text>
                                    </Button>
                                </Body>
                            </CardItem>
                        </Card> : <Spinner visible={loading} />
                    }
                </Content>
            </Container>
        );
    }
};
function mapDispatchToProps(dispatch) {
    return {
        getSingle: (id) => dispatch(fameActions.getSingle(id)),
        removeFav: (id) => dispatch(fameActions.removeFav(id)),
        addFav: (id) => dispatch(fameActions.addFav(id)),
    };
}

const mapStateToProps = (state) => {
    return {
        loading: state.fames.loading,
        single: state.fames.single,
        favs: state.fames.favs,
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ActorDetail);