import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, FlatList, ActivityIndicator, Image } from 'react-native';
import { Card, CardItem, Left, Right, Body, Thumbnail, Button, Icon, Container, Header, Content, Title } from 'native-base'
import fameAction from '../actions/fames';
import authAction from '../actions/auth';
import Clock from '../components/clock'

class Actors extends Component {
    state = {
        refreshing: false
    }
    async componentDidMount() {
        const { getFames } = this.props
        await getFames('1');
    }
    onLogout = async () => {
        const { logout } = this.props
        await logout()

    }
    onEndReached = async ({ distanceFromEnd }) => {
        const { getFames } = this.props
        await getFames('1')
    };
    onRefresh = async () => {
        const { getFames } = this.props
        await getFames()
    }
    keyExtractor = item => item.id;
    renderFooter = loading => {
        if (!loading) return null;

        return (
            <View>
                <ActivityIndicator animating size="large" />
            </View>
        );
    };
    onViewDetail = (id) => {
        const { navigation } = this.props
        navigation.navigate('actorDetail', { id })
    }
    renderItems = item => {
        return (
            <Card style={{ flex: 0 }}>
                <CardItem>
                    <Left>
                        <Thumbnail source={{ uri: item.image }} />
                        <Body>
                            <Text>{item.name}</Text>
                            <Text note>{item.dob}</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem>
                    <Left>
                        <Button transparent onPress={() => this.onViewDetail(item.id)}>
                            <Text>View Detail</Text>
                        </Button>
                    </Left>
                </CardItem>
            </Card >
        )
    }
    render() {
        const { loading, data } = this.props
        const { refreshing } = this.state
        return (
            <Container>
                <Header>
                    <Left />
                    <Body>
                        <Title>Header</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={this.onLogout}>
                            <Icon name='exit' />
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <Clock />
                    <FlatList
                        // onEndReached={status => this.onEndReached(status)}
                        onRefresh={this.onRefresh}
                        refreshing={loading}
                        // onEndReachedThreshold={1}
                        data={data}
                        ListFooterComponent={this.renderFooter(loading)}
                        renderItem={({ item }) => this.renderItems(item)}
                        keyExtractor={this.keyExtractor}
                    />
                </Content>
            </Container>
        );
    }
}
function mapDispatchToProps(dispatch) {
    return {
        getFames: (page) => dispatch(fameAction.getFames(page)),
        logout: () => dispatch(authAction.logout())
    };
}

const mapStateToProps = (state) => {
    return {
        loading: state.fames.loading,
        data: state.fames.data,
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Actors);
