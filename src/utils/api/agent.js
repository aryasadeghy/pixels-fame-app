import { create } from "apisauce";
import config from "../../../config";
import AsyncStorage from '@react-native-community/async-storage';
import { Toast } from 'native-base';


export const api = create({
    baseURL: config.apiUrl,
    timeout: 40000,
});

api.addAsyncRequestTransform(request => async () => {
    request.headers["Authorization"] = `Bearer ${await AsyncStorage.getItem(
        config.StorageToken
    )}`;
});
api.addResponseTransform(response => {
    if (response.status === 200 || response.status === 201) {
        return response;
    } else {
        throw response;
    }
});
// switch (!!response && response.status) {
//     case 201:
//         return response;
//     case 401:
//         throw response;
//     case 403:
//         throw response;
//     case 500:
//         throw response;
//     case 404:
//         throw response;
//     case 400:
//         throw response;
// }
