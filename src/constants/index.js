//auth type
export const SET_AUTH_TOKEN = 'SET_AUTH_TOKEN'
export const SET_AUTH_LOADING = 'SET_AUTH_LOADING'

//fames 

export const SET_FAMES = 'SET_FAMES'
export const FAMES_LOADING = 'FAMES_LOADING'
export const SET_SINGLE_FAME = 'SET_SINGLE_FAME'
export const ADD_FAV = 'ADD_FAV'
export const REMOVE_FAV = 'REMOVE_FAV'
