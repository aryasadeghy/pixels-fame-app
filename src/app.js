/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import Router from './routes';
import { Provider } from 'react-redux';
import NavigationService from './NavigationService';


import configureStore from './store/configureStore';

const store = configureStore()

const App = () => {
    return (
        <Provider store={store}>
            <Router
                ref={navigatorRef => {
                    NavigationService.setTopLevelNavigator(navigatorRef);
                }}
            />
        </Provider>
    );
};
export default App;
