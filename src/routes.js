import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

// import screens
import Login from './screens/login';
import Actors from './screens/actors';
import ActorsDetail from './screens/actorsDetail';
import Initial from './screens/initial';

const MainNavigator = createStackNavigator({
    initial: {
        screen: Initial,
        navigationOptions: {
            headerShown: false,
        },
    },
    login: {
        screen: Login,
        navigationOptions: {
            headerShown: false,
        },
    },
    actors: {
        screen: Actors,
        navigationOptions: {
            headerShown: false,
        },
    },
    actorDetail: { screen: ActorsDetail },
});

const Router = createAppContainer(MainNavigator);

export default Router;
