
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
export default class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hour: new Date().getHours().toLocaleString(),
            min: new Date().getMinutes().toLocaleString()
        };
    }
    componentDidMount() {
        this.intervalID = setInterval(
            () => this.tick(),
            1000
        );
    }
    componentWillUnmount() {
        clearInterval(this.intervalID);
    }
    tick() {
        this.setState({
            hour: new Date().getHours().toLocaleString(),
            min: new Date().getMinutes().toLocaleString()
        });
    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.clockText}>{this.state.hour} : {this.state.min}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop:10,
        marginBottom:10,
        flex: 1,
        justifyContent:'center',
        alignItems : 'center'
    },
    clockText : {
        fontSize:22
    }
})