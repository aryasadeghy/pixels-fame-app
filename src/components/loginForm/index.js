import React, { Component } from 'react';
import {
    Text,
} from 'react-native';
import { styles } from './styles'
import { connect } from 'react-redux';
import authActions from '../../actions/auth'
import Spinner from '../spinner'
import { Form, Item, Input, Button, Label } from 'native-base'
class LoginForm extends Component {
    state = {
        username: '',
        password: '',
    }


    onSubmit = () => {
        const { login, navigation } = this.props;
        const { username, password } = this.state
        console.log(username, password, 'ssssss')
        if (username && username.length > 0 && password && password.length > 0) {
            login({ username, password }, navigation)
        }
    }

    render() {
        const { loading } = this.props
        return (
            <Form>
                <Item floatingLabel>
                    <Label>Username</Label>
                    <Input onChangeText={(username) => this.setState({ username })} />
                </Item>
                <Item floatingLabel>
                    <Label>Password</Label>
                    <Input secureTextEntry={true} onChangeText={(password) => this.setState({ password })} />
                </Item>
                <Button style={[styles.buttonContainer, styles.loginButton]}
                    full onPress={this.onSubmit}>
                    <Text style={styles.loginText}>sumbit</Text>
                </Button>
                <Spinner visible={loading} />
            </Form>
        );
    }
}
function mapDispatchToProps(dispatch) {
    return {
        login: (values, navigation) => dispatch(authActions.login(values, navigation)),
    };
}

const mapStateToProps = (state) => {
    return {
        loading: state.auth.loading,
    };
};



export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
