import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    buttonContainer: {
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        borderRadius: 30,
    },
    loginButton: {
        backgroundColor: "#00b5ec",
    },
    loginText: {
        color: 'white',
    }
});