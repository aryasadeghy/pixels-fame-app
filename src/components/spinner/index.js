import React, {Component} from "react";
import Spinner from "react-native-loading-spinner-overlay";

export default class BillboardSpinner extends Component {
    static defaultProps = {
        textContent: "",
    };
    render() {
        const { textContent, visible, textStyle, ...props } = this.props;

        return (
            <Spinner
                {...props}
                visible={visible}
                textContent={textContent}
            />
        );
    }
}
