import { SET_SINGLE_FAME, SET_FAMES, FAMES_LOADING, ADD_FAV, REMOVE_FAV } from '../constants';
import { api } from '../utils/api/agent'
import * as  apiConstants from '../constants/api'

//seting reducers 
export function setFames(data) {
    return {
        type: SET_FAMES,
        payload: data,
    };
}
export function setSingleFame(data) {
    return {
        type: SET_SINGLE_FAME,
        payload: data,
    };
}
export function setLoading(status) {
    return {
        type: FAMES_LOADING,
        payload: status,
    };
}
export function addToFav(data) {
    return {
        type: ADD_FAV,
        payload: data,
    };
}
export function removeFromFav(data) {
    return {
        type: REMOVE_FAV,
        payload: data,
    };
}

// logic
const getFames = (page = 1) => {
    return async (dispatch) => {
        try {
            dispatch(setLoading(true));
            const response = await api.get(`${apiConstants.FAMES}?page=${page}`);
            await dispatch(setFames(response.data.data.list))
            await dispatch(setLoading(false))
        } catch (error) {
            console.error(error);
            dispatch(setLoading(false));
        }
    };
}
const getSingle = (id) => {
    return async (dispatch) => {
        try {
            dispatch(setLoading(true));
            const response = await api.get(`${apiConstants.FAMES}/${id}`);
            await dispatch(setSingleFame(response.data.data))
            await dispatch(setLoading(false))
        } catch (error) {
            console.error(error);
            dispatch(setLoading(false));
        }
    };
}
const addFav = (id) => {
    return async (dispatch) => {
        try {
            await dispatch(addToFav(id))
        } catch (error) {
            console.error(error);
        }
    };
}

const removeFav = (id) => {
    return async (dispatch, getState) => {
        try {
            const favs = getState().fames.fav
            const newFav = favs.find({ id })
            await dispatch(removeFav(newFav))
        } catch (error) {
        }
    };
}

export default {
    getFames,
    getSingle,
    removeFav,
    addFav
};