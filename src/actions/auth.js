import { SET_AUTH_TOKEN, SET_AUTH_LOADING } from '../constants';
import AsyncStroage from '@react-native-community/async-storage';
import NavigationService from '../NavigationService';
import { Alert } from 'react-native'
import config from '../../config'
import { api } from '../utils/api/agent';

import * as  apiConstants from '../constants/api'
export function setAuthToken(token) {
    return {
        type: SET_AUTH_TOKEN,
        payload: token,
    };
}
export function setLoading(status) {
    return {
        type: SET_AUTH_LOADING,
        payload: status,
    };
}
const login = (payload) => {
    return async (dispatch) => {
        try {
            dispatch(setLoading(true));
            const response = await api.post(apiConstants.LOGIN, payload);
            const token = response.headers.authorization
            if (response.data.data.success) {
                await AsyncStroage.setItem(config.StorageToken, token);
                await dispatch(setAuthToken(response.headers.authorization));
                NavigationService.navigate('actors');
            } else {
                Alert.alert('UserName or Password is Wrong')
            }
            dispatch(setLoading(false));
        } catch (error) {
            console.error(error);
            dispatch(setLoading(false));
        }
    };
}
const logout = () => {
    return async (dispatch) => {
        try {
            dispatch(setLoading(true));
            const response = await api.post(apiConstants.LOGOUT);
            if (response.data.data.success) {
                await AsyncStroage.removeItem(config.StorageToken);
                await dispatch(setAuthToken(undefined));
                NavigationService.navigate('login');
            } else {
                Alert.alert('Ops Sth Wrong')
            }
            dispatch(setLoading(false));


        } catch (error) {
            console.error(error);
            dispatch(setLoading(false));
        }
    };
}


export default {
    login,
    logout
};