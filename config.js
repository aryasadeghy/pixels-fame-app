export default {
    apiUrl: "https://halloffame-server.herokuapp.com/",
    StorageToken: '@hallAppStorage:access_token',
};
